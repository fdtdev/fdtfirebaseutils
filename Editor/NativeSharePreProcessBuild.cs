﻿using System.IO;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEngine;

public class NativeSharePreProcessBuild : IPreprocessBuildWithReport
{
    public int callbackOrder { get; }

    public void OnPreprocessBuild(BuildReport report)
    {
        var nativeShareGuids = AssetDatabase.FindAssets("nativeshare");
        if (nativeShareGuids.Length > 0)
        {
            var all = AssetDatabase.FindAssets("AndroidManifestSource");
            string path = AssetDatabase.GUIDToAssetPath(all[0]);
            var t = AssetDatabase.LoadAssetAtPath<TextAsset>(path);
            string c = t.text;
            c = c.Replace("%namespace%", Application.identifier);
            string newPath = Path.Combine(Application.dataPath, "Plugins/Android/NativeShare/AndroidManifest.xml");

            var newPathDir = Path.GetDirectoryName(newPath);

            if (!Directory.Exists(newPathDir))
            {
                Directory.CreateDirectory(newPathDir);
            }
            
            File.WriteAllText(newPath, c);
            AssetDatabase.Refresh();
        }
    }
}
