﻿using com.FDT.Analytics;
using com.FDT.Common.Editor;
using UnityEditor;
using UnityEngine;

namespace com.FDT.FirebaseUtils.Editor
{
    [CustomPropertyDrawer(typeof(AnalyticsVar))]
    public class AnalyticsVarDrawer : UnityEditor.PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            GUI.Box(position, GUIContent.none);
            position.x += 2;
            position.width -= 4;
            position.y += 2;
            position.height -= 4;
            
            EditorGUI.PropertyField(new Rect(position.x, position.y, position.width, 16), property.FindPropertyRelative("varName"), true);
            
            var tProp = property.FindPropertyRelative("varType");
            
            EditorGUI.PropertyField(new Rect(position.x, position.y+18, position.width, 16), tProp, true);
            var t = (AnalyticsVar.AnalyticsVarType) tProp.intValue;
            switch (t)
            {
                case AnalyticsVar.AnalyticsVarType.BOOL:
                    EditorGUI.PropertyField(new Rect(position.x, position.y+36, position.width, 16), property.FindPropertyRelative("boolCallback"), true);
                    break;
                case AnalyticsVar.AnalyticsVarType.INT:
                    EditorGUI.PropertyField(new Rect(position.x, position.y+36, position.width, 16), property.FindPropertyRelative("intCallback"), true);
                    break;
                case AnalyticsVar.AnalyticsVarType.FLOAT:
                    EditorGUI.PropertyField(new Rect(position.x, position.y+36, position.width, 16), property.FindPropertyRelative("floatCallback"), true);
                    break;
                case AnalyticsVar.AnalyticsVarType.STRING:
                    EditorGUI.PropertyField(new Rect(position.x, position.y+36, position.width, 16), property.FindPropertyRelative("stringCallback"), true);
                    break;
                case AnalyticsVar.AnalyticsVarType.LONG:
                    EditorGUI.PropertyField(new Rect(position.x, position.y+36, position.width, 16), property.FindPropertyRelative("intCallback"), true);
                    break;
                case AnalyticsVar.AnalyticsVarType.DOUBLE:
                    EditorGUI.PropertyField(new Rect(position.x, position.y+36, position.width, 16), property.FindPropertyRelative("floatCallback"), true);
                    break;
            }
            
            
            EditorGUI.EndProperty();
        }
        
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            float h = 18 * 2;
            var tProp = property.FindPropertyRelative("varType");
            var t = (AnalyticsVar.AnalyticsVarType) tProp.intValue;
            SerializedProperty p = null;
            switch (t)
            {
                case AnalyticsVar.AnalyticsVarType.BOOL:
                    p = property.FindPropertyRelative("boolCallback");
                    h += p.GetHeight();
                    break;
                case AnalyticsVar.AnalyticsVarType.INT:
                    p = property.FindPropertyRelative("intCallback");
                    h += p.GetHeight();
                    break;
                case AnalyticsVar.AnalyticsVarType.FLOAT:
                    p = property.FindPropertyRelative("floatCallback");
                    h += p.GetHeight();
                    break;
                case AnalyticsVar.AnalyticsVarType.STRING:
                    p = property.FindPropertyRelative("stringCallback");
                    h += p.GetHeight();
                    break;
                case AnalyticsVar.AnalyticsVarType.DOUBLE:
                    p = property.FindPropertyRelative("floatCallback");
                    h += p.GetHeight();
                    break;
                case AnalyticsVar.AnalyticsVarType.LONG:
                    p = property.FindPropertyRelative("intCallback");
                    h += p.GetHeight();
                    break;
            }
            return h + 4;
        }
    }
}
