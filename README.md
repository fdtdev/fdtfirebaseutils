# FDT Firebase Utils

Firebase utilities: Analytics, RemoteConfig


## Usage

The package must be included using the manifest.json file in the project.
The file must be modified to include this dependencies:

```json
{
  "dependencies": {
	"com.fdt.firebaseutils": "https://bitbucket.org/fdtdev/fdtfirebaseutils.git#1.0.0",
	"com.fdt.analytics": "https://bitbucket.org/fdtdev/fdtanalytics.git#1.0.0",
	"com.fdt.common": "https://bitbucket.org/fdtdev/fdtcommon.git#5.0.0",
	"com.fdt.gameevents": "https://bitbucket.org/fdtdev/fdtgameevents.git#2.0.0",
	"com.fdt.initializationhandler": "https://bitbucket.org/fdtdev/fdtinitializationhandler.git#2.0.0",

	...
  }
}

```

## License

MIT - see [LICENSE](https://bitbucket.org/fdtdev/fdtfirebaseutils/src/1.0.0/LICENSE.md)