﻿using System.Collections;
using com.FDT.InitializationHandler;
using UnityEngine;

namespace com.FDT.FirebaseUtils
{
    public class FirebaseInitHandler : InitWrapperItemBase
    {
        [SerializeField] protected FirebaseRemoteConfigAsset _config;

        public override void Init()
        {
            StartCoroutine(doInit());
        }

        protected IEnumerator doInit()
        {
            var e = Firebase.FirebaseApp.CheckAndFixDependenciesAsync();
            yield return e;
            if (e.Result == Firebase.DependencyStatus.Available)
            {
                _config.SetDefaultsFromCurrent();
                var a = _config.Update();
                yield return a;
                FinishInit();
            }
            else
            {
                UnityEngine.Debug.LogError(System.String.Format(
                    "Could not resolve all Firebase dependencies: {0}", e.Result));
                FinishInit();
            }
        }
    }
}