﻿using System;
using System.Collections;
using System.Collections.Generic;
using com.FDT.Common;
using UnityEngine;

namespace com.FDT.FirebaseUtils
{
    public abstract class FirebaseRemoteConfigAsset : ResetScriptableObject
    {
        [SerializeField] protected float _maxUpdateTime = 0.1f;

        public void SetDefaultsFromCurrent()
        {
            Dictionary<string, object> defaults = new Dictionary<string, object>();
            AddVars(defaults);
            Firebase.RemoteConfig.FirebaseRemoteConfig.SetDefaults(defaults);
        }

        protected abstract void AddVars(Dictionary<string, object> defaults);

        protected Coroutine updatingCoroutine = null;

        public FirebaseAsyncOp Update()
        {
            if (updatingCoroutine != null)
            {
                Debug.LogError("Firebase RemoteConfig already updating", this);
            }

            FirebaseAsyncOp result = new FirebaseAsyncOp();
            updatingCoroutine = Extensions.StartCoroutine(doUpdate(result));
            return result;
        }

        protected IEnumerator doUpdate(FirebaseAsyncOp asyncOp)
        {
            var a = Firebase.RemoteConfig.FirebaseRemoteConfig.FetchAsync(TimeSpan.FromSeconds(_maxUpdateTime));
            bool c = false;
            do
            {
                yield return null;
                c = a.IsCompleted || a.IsFaulted || a.IsCanceled;
            } while (!c);

            Debug.Log(
                $" Firebase RemoteConfig: Activating fetched data -> completed:{a.IsCompleted} | faulted:{a.IsFaulted} | iscanceled: {a.IsCanceled}");
            Firebase.RemoteConfig.FirebaseRemoteConfig.ActivateFetched();
            UpdateValues();
            updatingCoroutine = null;
            asyncOp.IsDone = true;
        }

        protected abstract void UpdateValues();

        protected string GetString(string key)
        {
            return Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(key).StringValue;
        }

        protected bool GetBool(string key)
        {
            return Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(key).BooleanValue;
        }

        protected double GetDouble(string key)
        {
            return Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(key).DoubleValue;
        }

        protected IEnumerable<byte> GetByteArray(string key)
        {
            return Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(key).ByteArrayValue;
        }

        protected long GetLong(string key)
        {
            return Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(key).LongValue;
        }

        protected int GetInt(string key)
        {
            return (int) Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(key).LongValue;
        }
    }


    public class FirebaseAsyncOp : CustomYieldInstruction
    {
        protected bool _isDone = false;

        public bool IsDone
        {
            get { return _isDone; }
            set { _isDone = value; }
        }

        public override bool keepWaiting
        {
            get { return !IsDone; }
        }
    }
}