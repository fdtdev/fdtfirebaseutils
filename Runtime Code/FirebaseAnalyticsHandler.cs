﻿using System;
using System.Collections.Generic;
using System.Globalization;
using com.FDT.Analytics;

namespace com.FDT.FirebaseUtils
{
    public class FirebaseAnalyticsHandler : AnalyticsHandlerBase
    {
        protected override void SendAnalytics(AnalyticEventAsset analyticsObj, Dictionary<string, object> parameters)
        {
            var analyticsParams = new Firebase.Analytics.Parameter[parameters.Count];
            int idx = 0;
            
            foreach (var p in parameters)
            {
                var analyticsVar = analyticsObj.GetAnalyticsVarByName(p.Key);
                string str = string.Empty;
                double d;
                switch (analyticsVar.varType)
                {
                    case AnalyticsVar.AnalyticsVarType.INT:
                    case AnalyticsVar.AnalyticsVarType.LONG:
                        analyticsParams[idx] = new Firebase.Analytics.Parameter(analyticsVar.varName, Convert.ToInt64(p.Value));
                        break;
                    case AnalyticsVar.AnalyticsVarType.BOOL:
                        analyticsParams[idx] = new Firebase.Analytics.Parameter(analyticsVar.varName, ((bool)p.Value == true?1:0) );
                        break;
                    case AnalyticsVar.AnalyticsVarType.FLOAT:
                    case AnalyticsVar.AnalyticsVarType.DOUBLE:
                        str = p.Value.ToString();
                        d = double.Parse( str, CultureInfo.InvariantCulture);
                        analyticsParams[idx] = new Firebase.Analytics.Parameter(analyticsVar.varName, d);
                        break;
                    case AnalyticsVar.AnalyticsVarType.STRING:
                        analyticsParams[idx] = new Firebase.Analytics.Parameter(analyticsVar.varName, (string)p.Value);
                        break;
                }

                idx++;
            }
            
            Firebase.Analytics.FirebaseAnalytics.LogEvent(
                 analyticsObj.analyticsEventName, analyticsParams
            );
        }
    }
}